
$(document).ready(function() {
    CALC.init();
    HISTORY.init();
    INTEGRAL.init();

    $('.numBtn, .operBtn').click(function() {
        var val = $(this).data('value');
        CALC.appendExpression(val);
        CALC.focusExpression();
    });

});

var CALC = {
    $expression: null,
    $backspace: null,
    $clear: null,

    // operations buttons
    $oper_Score: null,

    init: function() {
        this.initFields();
        this.initOperScore();
        this.initBackspace();
        this.initClear();
    },

    initFields: function() {
        this.$expression = $('#expression');
        this.$backspace = $('#backspace');
       this.$clear = $('#clear');

        // init operations buttons
        this.$oper_Score = $('[name=oper_Score]');
    },

    initOperScore: function() {
        var self = this;
        self.$oper_Score.click(function() {
            var query = self.$expression.val();
            var queryRequest = {
                expression: query
            }
            console.log(queryRequest);
            postJson('/query', queryRequest, function(data) {
                var result = data.responseText;
                self.setExpressionValue(result);
                HISTORY.addHistoryRow(query, result);
            });

        });
    },

    initBackspace: function() {
        var self = this;
        self.$backspace.click(function() {
            var cursorPosition = self.getExpressionCursorPosition();
            if (cursorPosition > 0) {
                var current = self.getExpressionValue();
                var newValue = current.slice(0, cursorPosition - 1) + current.slice(cursorPosition);
                self.setExpressionValue(newValue);
            }
            self.focusExpression();
        });
    },

    initClear: function() {
        var self = this;
        self.$clear.click(function() {
            self.setExpressionValue(null);
            self.focusExpression();
        });
    },

    getExpressionValue: function() {
        return this.$expression.val();
    },

    setExpressionValue: function(val) {
        this.$expression.val(val);
    },

    appendExpression: function(val) {
        var current = this.$expression.val();
        this.$expression.val(current + val);
    },

    focusExpression: function() {
        this.$expression.focus();
    },

    getExpressionCursorPosition: function() {
        return this.$expression.prop('selectionStart');
    },
};

var HISTORY = {
    $historyWrapper: null,

    init: function() {
        this.$historyWrapper = $('#history-wrapper');
    },

    addHistoryRow: function(query, result) {
        console.log('Adding new history row...');
        var $row = this.createHistoryRow(query, result);
        this.$historyWrapper.append($row);
        this.$historyWrapper.show();
    },

    createHistoryRow: function(query, result) {
        var $row = $('<div class="history-row"></div>');
        $row.append($('<span>' + query + '</span>'));
        $row.append($('<span>&nbsp;=&nbsp;</span>'));
        $row.append($('<strong>' + result + '</strong>'));
        return $row;
    }
};

var INTEGRAL = {
    $a: null,
    $b: null,
    $n: null,
    $submit: null,
    $integralResult: null,
    $integralTime: null,

    init: function() {
        var self = this;
        self.$a = $('#integral_a');
        self.$b = $('#integral_b');
        self.$n = $('#integral_n');
        self.$submit = $('#integral_submit');

        self.$integralResult = $('#integral_result');
        self.$integralTime = $('#integral_time');

        self.initIntegralSubmit();
    },

    initIntegralSubmit: function() {
        var self = this;
        self.$submit.click(function() {
            var a = parseInt(self.getA());
            var b = parseInt(self.getB());
            var n = parseInt(self.getN());
            if (isNaN(a) || isNaN(b) || isNaN(n)) {
                alert('Fill all inputs!');
            } else {
                if (a > b) {
                    alert('a parameter must be greater than b');
                } else {
                    if (n < 0) {
                        alert('Number of threads must be greater than 0');
                    } else {
                        var data = {
                            a: a,
                            b: b,
                            n: n
                        };
                        postJson('/integral', data, function(success) {
                            var json = success.responseJSON;
                            self.$integralResult.text(json.result);
                            self.$integralTime.text(json.time);
                        });
                    }
                }
            }
        });
    },

    getA: function() {
        return this.$a.val();
    },

    getB: function() {
        return this.$b.val();
    },

    getN: function() {
        return this.$n.val();
    },
};