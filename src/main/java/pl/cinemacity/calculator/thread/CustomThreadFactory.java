package pl.cinemacity.calculator.thread;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by pawel on 11.09.18 at 22:03
 */
public class CustomThreadFactory implements ThreadFactory {

    private final AtomicInteger integer = new AtomicInteger(1);

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, "thread" + integer.getAndIncrement());
    }
}
