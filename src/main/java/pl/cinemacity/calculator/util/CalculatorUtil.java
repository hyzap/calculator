package pl.cinemacity.calculator.util;

import lombok.Data;
import org.apache.logging.log4j.util.Strings;
import pl.cinemacity.calculator.exception.CalculatorException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pawel on 10.09.18 at 20:40
 */
public final class CalculatorUtil {

    private static final String SCRIPT_ENGINE_NAME = "javascript";

    private static final char CODE_FROM_UI_ADD = '+';
    private static final char CODE_FROM_UI_SUBTRACT = '-';
    private static final char CODE_FROM_UI_MULTIPLY = '×';
    private static final char CODE_FROM_UI_DIVIDE = '÷';
    private static final char CODE_FROM_UI_SQRT = '√';
    private static final char CODE_FROM_UI_POWER = '^';

    private static final char BRACKET_ROUND_OPEN = '(';
    private static final char BRACKET_ROUND_CLOSE = ')';
    private static final char BRACKET_SQUARE_OPEN = '[';
    private static final char BRACKET_SQUARE_CLOSE = ']';

    private static final String CODE_FROM_UI_MULTIPLY_STR = "×";
    private static final String CODE_FROM_UI_DIVIDE_STR = "÷";
    private static final String CODE_FROM_UI_SQRT_STR = "√";
    private static final String CODE_FROM_UI_POWER_STR = "^";

    private static final String BRACKET_ROUND_OPEN_STR = "(";
    private static final String BRACKET_ROUND_CLOSE_STR = ")";
    private static final String BRACKET_SQUARE_OPEN_STR = "\\[";
    private static final String BRACKET_SQUARE_CLOSE_STR = "]";

    private static final char SPACE = ' ';
    private static final char COMMA = ',';

    private static final String MATH_POWER = "Math.pow";
    private static final String MATH_SQRT = "Math.sqrt";

    private static final List<Character> OPERATORS_AFTER_POWER = Arrays.asList(
            CODE_FROM_UI_ADD,
            CODE_FROM_UI_SUBTRACT,
            CODE_FROM_UI_MULTIPLY,
            CODE_FROM_UI_DIVIDE,
            CODE_FROM_UI_SQRT,
            CODE_FROM_UI_POWER,
            BRACKET_ROUND_OPEN,
            BRACKET_ROUND_CLOSE,
            BRACKET_SQUARE_OPEN,
            BRACKET_SQUARE_CLOSE,
            SPACE);

    @Data
    static class Brackets {
        private int start = -1;
        private int end = -1;

        public boolean isEmpty() {
            return start == -1 && end == -1;
        }
    }

    public static String calculateExpression(String expression) throws CalculatorException {
        String convertedExpression = convertExpression(expression);
        System.out.println(convertedExpression);
        Object out = calculateUsingScriptEngine(convertedExpression);
        return out.toString();
    }

    private static Object calculateUsingScriptEngine(String expression) throws CalculatorException {
        checkOperationsBeforeEval(expression);
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName(SCRIPT_ENGINE_NAME);
        try {
            return engine.eval(expression);
        } catch (ScriptException e) {
            throw new CalculatorException("Could not execute calculation", e);
        }
    }

    private static void checkOperationsBeforeEval(String expression) throws CalculatorException {
        checkIfContainsAndThrowException(expression, CODE_FROM_UI_MULTIPLY_STR);
        checkIfContainsAndThrowException(expression, CODE_FROM_UI_DIVIDE_STR);
        checkIfContainsAndThrowException(expression, CODE_FROM_UI_SQRT_STR);
        checkIfContainsAndThrowException(expression, CODE_FROM_UI_POWER_STR);
        checkIfContainsAndThrowException(expression, BRACKET_SQUARE_OPEN_STR);
        checkIfContainsAndThrowException(expression, BRACKET_SQUARE_CLOSE_STR);
    }

    private static void checkIfContainsAndThrowException(String expression, String valueToBeChecked)
            throws CalculatorException {
        if (expression.contains(valueToBeChecked)) {
            throw new CalculatorException(
                    String.format("Expression should not contains [%s] before engine evaluation", valueToBeChecked));
        }
    }

    private static String convertExpression(String expression) {
        String cleanedExpression = getConvertedExpressionFromSpaceToEmptyChar(expression);
        cleanedExpression = getConvertedExpressionFronUiSimpleOperators(cleanedExpression);

        Brackets brackets = getFirstBrackets(cleanedExpression);
        char[] cleanedExpressionArray = cleanedExpression.toCharArray();

        if (!brackets.isEmpty()) {
            cleanedExpressionArray[brackets.start] = BRACKET_SQUARE_OPEN;
            cleanedExpressionArray[brackets.end] = BRACKET_SQUARE_CLOSE;
        }

        cleanedExpression = new String(cleanedExpressionArray);
        cleanedExpression = getConvertedExpressionForSqrtOperation(brackets, cleanedExpression);
        cleanedExpression = getConvertedExpressionForPowerOperation(brackets, cleanedExpression);

        if (hasExpressionOperationsInBracket(cleanedExpression)) {
            return convertExpression(cleanedExpression);
        }

        return getConvertedExpressionFromSquareToRoundBrackets(cleanedExpression);
    }

    private static boolean hasExpressionOperationsInBracket(String expression) {
        return expression.contains(String.valueOf(BRACKET_ROUND_OPEN));
    }

    /**
     * for example:
     * expression:     a^xyz - d
     * startIndex:     2
     * result is:
     * 4
     */
    private static int getPowerEndIndex(String expression, int startIndex) {
        char[] tmp = expression.toCharArray();
        int endIndex = tmp.length - 1;
        for (int i = startIndex + 1; i < tmp.length; i++) {
            if (OPERATORS_AFTER_POWER.contains(tmp[i])) {
                endIndex = i - 1;
                break;
            }
        }
        return endIndex;
    }

    /**
     * for example:
     * expression: a + (c - (d * (e - f)) - z) + y
     * result is:
     * { start: 4, end: 26 }
     */
    private static Brackets getFirstBrackets(String expression) {
        if (doesExpressionContainsPairOfRoundBrackets(expression)) {
            Brackets brackets = new Brackets();
            brackets.start = expression.indexOf(BRACKET_ROUND_OPEN);
            int bracketsStarts = 1;
            char[] expressionArray = expression.toCharArray();

            for (int i = brackets.start + 1; i < expressionArray.length; i++) {
                if (expressionArray[i] == BRACKET_ROUND_OPEN) {
                    bracketsStarts++;
                } else if (expressionArray[i] == BRACKET_ROUND_CLOSE) {
                    bracketsStarts--;
                    if (bracketsStarts == 0) {
                        brackets.end = i;
                        break;
                    }
                }
            }

            return brackets;
        }
        return new Brackets();
    }

    private static boolean doesExpressionContainsPairOfRoundBrackets(String expression) {
        return expression.contains(BRACKET_ROUND_OPEN_STR)
                && expression.contains(BRACKET_ROUND_CLOSE_STR);
    }

    private static String getConvertedExpressionFromSpaceToEmptyChar(String expression) {
        return expression.replaceAll(String.valueOf(SPACE), Strings.EMPTY);
    }

    private static String getConvertedExpressionFromSquareToRoundBrackets(String expression) {
        return expression
                .replaceAll(BRACKET_SQUARE_OPEN_STR, BRACKET_ROUND_OPEN_STR)
                .replaceAll(BRACKET_SQUARE_CLOSE_STR, BRACKET_ROUND_CLOSE_STR);
    }

    private static String getConvertedExpressionFronUiSimpleOperators(String expression) {
        return expression.replaceAll(CODE_FROM_UI_MULTIPLY_STR, "*")
                .replaceAll(CODE_FROM_UI_DIVIDE_STR, "/");
    }

    /**
     * for example:
     * expression:     a + √(b+c) - d
     * brackets:       { start: 5, end: 9 }
     * result is:
     * a + Math.sqrt[(b+c)] - d
     */
    private static String getConvertedExpressionForSqrtOperation(Brackets brackets, String expression) {
        if (brackets.start > 0) {
            boolean isSqrtOperation = expression.charAt(brackets.start - 1) == CODE_FROM_UI_SQRT;
            if (isSqrtOperation) {
                String before = expression.substring(0, brackets.start - 1);
                String after = expression.substring(brackets.start);
                return before + MATH_SQRT + after;
            }
        }
        return expression;
    }

    /**
     * for example:
     * expression:     a - (b)^c + d
     * brackets:       { start: 4, end: 6 }
     * result is:
     * a - Math.pow[(b), c] + d
     */
    private static String getConvertedExpressionForPowerOperation(Brackets brackets, String expression) {
        boolean hasEnoughChars = expression.length() > brackets.end + 1;
        if (hasEnoughChars) {
            boolean isPowerOperation = expression.charAt(brackets.end + 1) == CODE_FROM_UI_POWER;
            if (isPowerOperation) {
                String before = expression.substring(0, brackets.start);
                String operation = expression.substring(brackets.start, brackets.end + 1);
                int powerEndIndex = getPowerEndIndex(expression, brackets.end + 2);
                String after = expression.substring(powerEndIndex + 1);
                String power = expression.substring(brackets.end + 2, powerEndIndex + 1);
                return before + MATH_POWER + BRACKET_SQUARE_OPEN + operation + COMMA + SPACE + power + BRACKET_SQUARE_CLOSE + after;
            }
        }
        return expression;
    }
}
