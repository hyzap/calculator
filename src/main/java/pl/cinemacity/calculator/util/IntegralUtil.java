package pl.cinemacity.calculator.util;

import pl.cinemacity.calculator.constants.IntegralRequest;
import pl.cinemacity.calculator.exception.CalculatorException;
import pl.cinemacity.calculator.thread.CustomThreadFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by pawel on 11.09.18 at 20:49
 */
public final class IntegralUtil {

    private static final BigDecimal E = BigDecimal.valueOf(Math.E);

    private static BigDecimal f(BigDecimal x) {
        BigDecimal out = BigDecimal.ONE;
        for (int i = 0; i < x.intValue(); i++) {
            out = out.multiply(E);
        }
        return out;
    }

    public static BigDecimal calculate(IntegralRequest request) throws CalculatorException {
        final BigDecimal N = BigDecimal.valueOf(request.getB() - request.getA());
        final BigDecimal a = BigDecimal.valueOf(request.getA());
        final BigDecimal b = BigDecimal.valueOf(request.getB());
        final BigDecimal dx = b.subtract(a).divide(N);
        final ExecutorService executor = Executors.newFixedThreadPool(request.getN(), new CustomThreadFactory());
        final List<Future<BigDecimal>> list = new ArrayList<>();

        BigDecimal s = BigDecimal.ZERO;

        for (int i = 1; i <= N.intValue(); i++) {
            final BigDecimal bi = BigDecimal.valueOf(i);
            list.add(executor.submit(() -> f(a.add(dx.multiply(bi)))));
        }

        List<BigDecimal> parts = Collections.synchronizedList(new ArrayList<>());

        for (Future<BigDecimal> fut : list) {
            try {
                parts.add(fut.get());
            } catch (InterruptedException | ExecutionException e) {
                throw new CalculatorException("Error when calculating integral");
            }
        }

        executor.shutdown();

        for (BigDecimal part : parts) {
            s = s.add(part);
        }

        return s.multiply(dx);
    }
}
