package pl.cinemacity.calculator.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.cinemacity.calculator.constants.IntegralRequest;
import pl.cinemacity.calculator.constants.IntegralResponse;
import pl.cinemacity.calculator.exception.CalculatorException;
import pl.cinemacity.calculator.util.CalculatorUtil;
import pl.cinemacity.calculator.util.IntegralUtil;

import java.math.BigDecimal;

/**
 * Created by pawel on 09.09.18 at 18:07
 */
@Controller
public class StartController {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class QueryRequest {
        private String expression;
    }

    @GetMapping(value = "/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @ResponseBody
    @PostMapping(value = "/query")
    public String query(@RequestBody QueryRequest query) {
        try {
            System.out.println(query);
            return CalculatorUtil.calculateExpression(query.getExpression());
        } catch (Throwable e) {
            e.printStackTrace();
            return "Error";
        }
    }

    @ResponseBody
    @PostMapping(value = "/integral")
    public IntegralResponse integral(@RequestBody IntegralRequest request) throws CalculatorException {
        long start = System.currentTimeMillis();
        BigDecimal result = IntegralUtil.calculate(request);
        System.out.println("Integral result: " + result);
        return new IntegralResponse(result, System.currentTimeMillis() - start);
    }
}
