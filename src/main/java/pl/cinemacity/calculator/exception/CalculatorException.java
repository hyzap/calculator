package pl.cinemacity.calculator.exception;

/**
 * Created by pawel on 11.09.18 at 19:12
 */
public class CalculatorException extends Exception {

    public CalculatorException(String msg) {
        super(msg);
    }

    public CalculatorException(String msg, Throwable e) {
        super(msg, e);
    }
}
