package pl.cinemacity.calculator.constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by pawel on 11.09.18 at 20:51
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IntegralRequest {
    private int a;
    private int b;
    private int n;
}
