package pl.cinemacity.calculator.constants;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by pawel on 11.09.18 at 20:55
 */
@Data
@AllArgsConstructor
public class IntegralResponse {
    private BigDecimal result;
    private long time;
}
