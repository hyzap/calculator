package pl.cinemacity.calculator;

import org.junit.Test;
import pl.cinemacity.calculator.exception.CalculatorException;
import pl.cinemacity.calculator.util.CalculatorUtil;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pawel on 10.09.18 at 21:27
 */
public class CalculatorUtilTest {

    private static final Map<String, Double> TEST_MAP = new HashMap<>();

    static {
        TEST_MAP.put("1+2", 3.0);
        TEST_MAP.put("1+(2)", 3.0);
        TEST_MAP.put("(1)+(2)", 3.0);
        TEST_MAP.put("(1+2)", 3.0);
        TEST_MAP.put("2+2×2", 6.0);
        TEST_MAP.put("(2+2)×2", 8.0);
        TEST_MAP.put("(2+2)×2+1", 9.0);
        TEST_MAP.put("(2+2)×(2+1)", 12.0);
        TEST_MAP.put("(2+2)×(1-2)", -4.0);
        TEST_MAP.put("100-50×2", 0.0);
        TEST_MAP.put("(100-50)×2", 100.0);
        TEST_MAP.put("1+(1-1×1)", 1.0);
        TEST_MAP.put("1+(1-1×1)-((1)^9999999-1)", 1.0);
        TEST_MAP.put("(1+2)^2", 9.0);
        TEST_MAP.put("(1+2)^2-√(9)", 6.0);
        TEST_MAP.put("(1+2)^2-√(27÷3)", 6.0);
        TEST_MAP.put("√(√(√(√(65536))))", 2.0);
        TEST_MAP.put("27÷9+1", 4.0);
        TEST_MAP.put("27÷(8+1)", 3.0);
    }

    @Test
    public void calculatorTestSuccess() {
        TEST_MAP.forEach((expression, result) -> assertTestPair(expression, result));
    }

    @Test(expected = CalculatorException.class)
    public void calculatorTestSqrtError() throws CalculatorException {
        CalculatorUtil.calculateExpression("√9"); // it should be √(9)
    }

    @Test(expected = CalculatorException.class)
    public void calculatorTestPowerError() throws CalculatorException {
        CalculatorUtil.calculateExpression("5^2"); // it should be (5)^2
    }

    private void assertTestPair(String expression, Double result) {
        try {
            Double fromUtil = Double.parseDouble(CalculatorUtil.calculateExpression(expression));
            assertThat(fromUtil).isEqualTo(result);
        } catch (CalculatorException e) {
            throw new RuntimeException(e);
        }
    }
}
